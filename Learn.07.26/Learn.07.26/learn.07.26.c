//#include<stdio.h>
//union Un
//{
//	int i;
//	char c;
//};
//int main()
//{
//	union Un un;
//	//联合的成员是共用同一块内存空间的，这样一个联合变量的大小，至少是最大成员的大小（因为联
//	//合至少得有能力保存最大的那个成员）
//	// 下面输出的结果是一样的吗？
//	printf("%d\n", &(un.i));
//	printf("%d\n", &(un.c));
//	//下面输出的结果是什么？
//	un.i = 0x11223344;
//	un.c = 0x55;
//	printf("%x\n", un.i);
//}




//#include<stdio.h>
//union Un1
//{
//	char c[5];
//	int i;
//};
//union Un2
//{
//	short c[7];
//	int i;
//};
//int main()
//{
//	//下面输出的结果是什么？
//	printf("%d\n", sizeof(union Un1));
//	printf("%d\n", sizeof(union Un2));
//}


//#include<stdio.h>
//int main()
//{
//	enum Color//颜色
//	{
//		RED = 1,
//		GREEN = 2,
//		BLUE = 4
//	};
//	enum Color clr = GREEN;
//	//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异。
//	clr = 5;//ok??
//}