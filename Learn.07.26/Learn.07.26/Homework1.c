//day1编程题
//输入数字 n，按顺序打印出从 1 到最大的 n 位十进制数。
// 比如输入 3，则打印出 1、2、3 一直到最大的 3 位数 999。
//1. 用返回一个整数列表来代替打印
//2. n 为正整数，0 < n <= 
//输入：
//1
//返回值：
//[1, 2, 3, 4, 5, 6, 7, 8, 9]

//static int a[100010];
//int* printNumbers(int n, int* returnSize) {
//    int k = 1;
//    for (int i = 0; i < n; i++)
//        k *= 10;
//    int i;
//    for (i = 1; i < k; i++)
//        a[i - 1] = i;
//    *returnSize = --i;
//    return a;
//}


//计算日期到天数转换
//
//描述
//根据输入的日期，计算是这一年的第几天。
//保证年份为4位数且日期合法。
//进阶：时间复杂度：O(n)\O(n) ，空间复杂度：O(1)\O(1)
//输入描述：
//输入一行，每行空格分割，分别是年，月，日
//
//输出描述：
//输出是这一年的第几天
//示例1
//输入：
//2012 12 31
//复制
//输出：
//366

//#include<stdio.h>
//int main()
//{
//    int year = 0;
//    int month = 0;
//    int day = 0;
//    scanf("%4d %d %d", &year, &month, &day);
//    int arr[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//    {
//        arr[1] += 1;
//    }
//    int i = 0;
//    int sum = day;
//    for (i = 0; i < month - 1; i++)
//    {
//        sum += arr[i];
//    }
//    printf("%d\n", sum);
//    return 0;
//}


#include<stdio.h>
int main()
{
	char s[] = "\\123456\123456\t";
	printf("%d\n", strlen(s));
	return 0;
}
