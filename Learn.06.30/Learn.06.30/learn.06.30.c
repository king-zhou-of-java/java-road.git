////指针未初始化
////#include <stdio.h>
////int main()
////{
////	int* p;//局部变量指针未初始化，默认为随机值
////	*p = 20;
////	return 0;
////}////////2. 指针越界访问
////3. 指针指向的空间释放
////3.2 如何规避野指针
////1. 指针初始化
////2. 小心指针越界
////3. 指针指向空间释放，及时置NULL
////4. 避免返回局部变量的地址
////5. 指针使用之前检查有效性
//
//
////#include <stdio.h>
////int main()
////{
////	int arr[10] = { 0 };
////	int* p = arr;
////	int i = 0;
////	for (i = 0; i <= 11; i++)
////	{
////		//当指针指向的范围超出数组arr的范围时，p就是野指针
////		*(p++) = i;
////	}
////	return 0;
////}//////////// 指针-指针////#include<stdio.h>////int main()////{////	int  arr[10] = { 0 };////	printf("%d\n",&arr[9] - &arr[0]);//// //输出为9 指针-指针得到的是指针之间元素的个数////	return 0;////}////
//////指针+-整数
////#define N_VALUES 5
////float values[N_VALUES];
////float* vp;
//////指针+-整数；指针的关系运算
////for (vp = &values[0]; vp < &values[N_VALUES];)
////{
////	*vp++ = 0;
////	//等价于
////	*vp = 0;
////	vp++;
////}////#include<stdio.h>////int main()////{////	int arr[10] = { 0 };////	int* p = arr;////	int sz = sizeof(arr) / sizeof(arr);////	for (int i = 0; i < sz; i++)////	{////		printf("%d ", *(p + i));////	}////	return 0;////}