//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "abcdef";//arr里面放的abcdef\0
//	int len = strlen(arr); 
//	printf("%d", len);
//	char arr1[] = { 'b','i','t' };
//	int len1 = strlen(arr1);//随机值
//	//返回类型是size_t  是 unsigned int 的类型
//}
//
//
//
//
////模拟实现strlen
////计数器方法
//#include<stdio.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	size_t count = 0;
//	assert(str);
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcdef";
//	size_t n = my_strlen(arr);
//	printf("%u\n", n);
//	return 0;
//}
//
//
//
//#include<stdio.h>
//int main()
//{
//	char arr[100] = { 0 };//源字符串必须以 '\0' 结束。
//	strcpy(arr, "zhangsan");//会将源字符串中的 '\0' 拷贝到目标空间。
//	printf("%s", arr);
//}
//
//模拟实现strcpy  
//#include<stdio.h>
//#include<assert.h>
//char* my_strcpy(char*arr2,const char*arr1)
//{
//	assert(arr1);
//	assert(arr2);
//	char* ret=arr2;
//	while (*arr2++ = *arr1++)//这样写可以保证arr1里面的'\0'可以给到arr2里面
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}
//
//
// 
// 
//模拟实现strcat
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = "hello ";
//	my_strcat(arr1, "world");
//	printf("%s\n", arr1);
//	return 0;
//}
//
//
//strcmp
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "zhangsan";
//	char arr2[] = "zhangsanfeng";
//	int ret = strcmp(arr1, arr2);
//	if (ret > 0)
//	{
//		printf(">\n");
//	}
//	else if (ret < 0)
//	{
//		printf("<\n");
//	}
//	else
//		printf("==\n");
//}
//
//
//
//
//模拟实现strcmp
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//int my_strcmp(const char *str1,const char*str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')//这里是相等的情况 因为比较到都遇到'\0'了 所以返回0
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//		return 1;
//	else
//		return -1;
//}
//int main()
//{
//	char arr1[] = "zhangsan";
//	char arr2[] = "zhangsanfeng";
//	int ret = my_strcmp(arr1, arr2);
//	if (ret > 0)
//	{
//		printf(">\n");
//	}
//	else if (ret < 0)
//	{
//		printf("<\n");
//	}
//	else
//		printf("==\n");
//}
//
//
//
//
//
//
//长度受限制的字符串函数介绍   VS自带的带N的版本 只能在vs的环境下运行
// strncat 字符串追加前n个 在追加的末尾会补上一个\0 
//strncpy  字符串拷贝前n个
//strncmp   字符串比较前n个字符
//#include<stdio.h>
//int main()
//{
//	char arr1[20] = "abcedf";
//	char arr2[] = "hello bit";
//	int ret = strncpy(arr1, arr2, 5);//拷贝前五个字符
//	printf("%d\n", ret);
//	return 0;
//
//}
//
//
//
//     strstr
////这里返回找到的第一个字符以及之后的所有字符 在一个函数里找子字符串
//#include<stdio.h>
//int main()
//{
//	char email[] = "zpw@bitejiuyeke.com";
//	char sub[] = "bitejiuyeke";
//	char* ret = strstr(email, sub);
//	if (ret == NULL)
//	{
//		printf("子串不存在");
//	}
//	else
//		printf("%s\n", ret);//返回结果bitejiuyeke.com
//	return 0;
//}
// 
// 
// 
// 
// 
// 
//模拟实现strstr
//情况1  abcdef  abc   情况2  abbbcdef  bbc  （多次匹配才能找到）
//#include<stdio.h>
//#include<assert.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	const char* s1 = str1;
//	const char* s2 = str2;
//	const char* p = str1;
//	while (*p)
//	{
//		s1 = p;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return p;
//		}
//		p++;
//		return NULL;
//	}
//}
//int main()
//{
//	char email[] = "com";
//	char sub[] = "c";
//	char* ret = strstr(email, sub);
//	if (ret == NULL)
//	{
//		printf("子串不存在");
//	}
//	else
//		printf("%s\n", ret);//返回结果bitejiuyeke.com
//	return 0;
//}
//
//
//
//
//strtok
//#define _CRT_SECURE_NO_WARNINGS
//
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "mianxiang@dachangbiancheng.com";
//	const char* sep = "@.";
//	char cp[50] = { 0 };
//	strcpy(cp, arr);
//	char* ret = NULL;
//
//	简便方法
//	for (ret = strtok(cp, sep); ret != NULL; ret = strtok(NULL, sep))
//	{
//		printf("%s\n", ret);
//	}
//
//	挫方法
//	printf("%s\n", ret);
//	ret = strtok(NULL, sep);
//	printf("%s\n", ret);
//	ret = strtok(NULL, sep);
//	printf("%s\n", ret);
//	return 0;
//
//}


#define _CRT_SECURE_NO_WARNINGS

//#include<stdio.h>
//#include<errno.h>
////errno 把错误码转换成对应的错误信息输出
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	else
//	{
//		;
//	}
//	return 0;
//}


//    memcpy
//#include<stdio.h>
//#include<assert.h>
//void* my_memcpy(void* dest, void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	while (num--)
//	{
//		*(char* )dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7};
//	int arr2[10] = { 0 };
//	my_memcpy(arr2, arr1, 28);
//	return 0;
//}




//模拟实现memmove
//和memcpy的差别就是memmove函数处理的源内存块和目标内存块是可以重叠的
//#include<stdio.h>
//#include<assert.h>
//void* my_memmove(void* dest, const void* src,size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	if (dest < src)//前->后
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		*((char*)dest + num) = *((char*)src + num);
//	}
//}
//void test3()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr1, arr1 + 2,20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//}
//int main()
//{
//	test3();
//	return 0;
//}



//memcmp   比较字节个数
//#include<stdio.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,3 };
//	int ret = memcmp(arr1, arr2, 12);
//	printf("%d\n",ret);
//}



//memset
#include<stdio.h>
int main()
{
	char arr[] = "hello world";
	memset(arr, 'x', 5);//x是要覆盖的元素  5是覆盖的字节数
	printf("%s\n", arr);
}