﻿//#include<stdio.h>
//int main()
//{
//	int arr[5];
//	int (*pa)[5] = &arr;
// void (*signal(int , void(*)(int)))(int);
//	// 代码简化-->把void (*)(int)重命名为pfun_t
//	typedef void(*pfun_t)(int);
//	pfun_t signal(int, pfun_t);
//	//函数指针的用法
//
//}
// 
// //写一个计算器
	//加法
	//减法
	//除法
	//乘法
//#include<stdio.h>
//void menu()
//{
//	printf("******************");
//	printf("1.add 2.sub");
//	printf("3.mul; 4.div");
//	printf("0.exit");
//}
//int main()
//{
//	int input = 0;
//	do
//	{
//		int x, y;
//		menu();
//		printf("请选择：");
//		scanf("%d", &input);
//		printf("请输入两个操作数：");
//		scanf("%d%d", &x, &y);
//		swich(input)
//		{
//			case 1:
//				break;
//			case 2:
//				break;
//			case 3：
//			    break;
//
//		}
//	}
//	int  calc(int (*pf)(int, int))
//
//	{
//		scanf("%d%d", &x, &y);
//		int ret = pf(x, y);
//	}
//}
//int (*arr[4])(int, int) = { add,su,ad };//函数指针数组
#include <stdio.h>
int add(int a, int b)
{
	return a + b;
}
int sub(int a, int b)
{
	return a - b;
}
int mul(int a, int b)
{
	return a * b;
}
int div(int a, int b)
{
	return a / b;
}
int main()
{
	int x, y;
	int input = 1;
	int ret = 0;
	int(*p[5])(int x, int y) = { 0, add, sub, mul, div }; //转移表
	while (input)
	{
		printf("*************************\n");
		printf(" 1:add           2:sub \n");
		printf(" 3:mul           4:div \n");
		printf("*************************\n");
		printf("请选择：");
		scanf("%d", &input);
		if ((input <= 4 && input >= 1))
		{
			printf("输入操作数：");
			scanf("%d %d", &x, &y);
			ret = (*p[input])(x, y);
		}
		else
			printf("输入有误\n");
		printf("ret = %d\n", ret);
	}
	return 0;
}
void test(const char* str)
{
	printf("%s\n", str);
}
int main()
{
	//函数指针pfun
	void (*pfun)(const char*) = test;
	//函数指针的数组pfunArr
	void (*pfunArr[5])(const char* str);
	pfunArr[0] = test;
	//指向函数指针数组pfunArr的指针ppfunArr
	void (*(*ppfunArr)[5])(const char*) = &pfunArr;
	return 0;
}