//#include <stdio.h>
////数组名
///*1. sizeof(数组名)，计算整个数组的大小，sizeof内部单独放一个数组名，数组名表示整个数
//组。
//2. & 数组名，取出的是数组的地址。 & 数组名，数组名表示整个数组。
//除此1, 2两种情况之外，所有的数组名都表示数组首元素的地址。*/
//
//int main()
//{
//	int arr[10] = {1,2,3,4,5};
//	printf("%p\n", arr);
//	printf("%p\n", &arr[0]);
//	printf("%d\n", *arr);
//	int arr[10] = { 0 };
//	printf("%d\n", sizeof(arr));
//	//输出结果
//	return 0;
//}
