/*输入：
2 3
1 2 3
4 5 6
复制
输出：
1 4 
2 5 
3 6 */
//矩阵转置
#define _CRT_SECURE_NO_WARNINGS

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int arr[10][10] = { 0 };
//    scanf("%d %d", &n, &m);
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    for (i = 0; i < m; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            printf("%d ", arr[j][i]);
//        }
//        printf("\n");
//    }
//
//    return 0;
//}


//上三角矩阵判定
/*输入：
3
1 2 3 1
0 4 5 5
0 0 6 8
0 0 0 6
复制
输出：
YES*/
//#include<stdio.h>
//int main()
//{
//	int s = 0;
//	int arr[20][20] = { 0 };
//	scanf("%d", &s);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < s; i++)
//	{
//		for (j = 0; j < s; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	int sum = 0;
//	for (i = 0; i < s; i++)
//	{
//		for (j = 0; j < s; j++)
//		{
//			if (i > j)
//			{
//				sum += arr[i][j];
//			}
//		}
//	}
//	if (sum == 0)
//	{
//		printf("YES\n");
//	}
//	else
//		printf("NO\n");
//	return 0;
//}





//有序序列判断
/*输入描述：
第一行输入一个整数N(3≤N≤50)。
第二行输入N个整数，用空格分隔N个整数。
输出描述：
输出为一行，如果序列有序输出sorted，否则输出unsorted。
示例1
输入：
5
1 6 9 22 30
复制
输出：
sorted*/
#include <stdio.h>
int main()
{
    int a[55], n, flag1 = 0, flag2 = 0, i;
    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        if (i > 0) {
            if (a[i] < a[i - 1]) {
                flag1 = 1;
            }
            else if (a[i] > a[i - 1]) {
                flag2 = 1;
            }
        }
    }
    if (flag1 && flag2)
        printf("unsorted\n");//只有当flag1和flag2都为1的时候序列无序
    else
        printf("sorted\n");
}