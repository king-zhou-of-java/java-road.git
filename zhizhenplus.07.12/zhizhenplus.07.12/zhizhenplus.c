//第一题
//#include<stdio.h>
//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));//（2,5）
//	return 0;
//}


//第二题
//由于还没学习结构体，这里告知结构体的大小是20个字节
//#include<stdio.h>
//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p=(struct Test*)0x100000;
////假设p 的值为0x100000。 如下表表达式的值分别为多少？
////已知，结构体Test类型的变量大小是20个字节
//int main()
//{
//	//注意这里的+1是结构体大小的+1，因为结构体Test类型的变量大小是20个字节
//	//所以0x100000+20-->   0x100014（最终结果)
//	printf("%p\n", p + 0x1);
//
//	//0x100000转换为十进制为1048576 然后+1 = 1048577
//	//再转换为十六进制就是0x100000+1 = 0x100001（最终结果）
//	printf("%p\n", (unsigned long)p + 0x1);
//
//	 
//	//注意这里强制类型转换成无符号整型 +1 即跳过四个字节 -->0x100004（最终结果)
//	printf("%p\n", (unsigned int*)p + 0x1);
//	return 0;
//}


//第三题 
//#include<stdio.h>
//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x", ptr1[-1], *ptr2);// 4,2000000
//	return 0;
//}


//第四题
//#include <stdio.h>
//int main()
//{
//	//注意这个数组里面的元素是逗号表达式 从左往右计算 
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) };
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);//结果 1
//	return 0;
//}


//第五题
//#include<stdio.h>
//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	//最终结果 FFFFFFFC,-4
//	return 0;
//}


//第六题
//#include<stdio.h>
//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));//10，5
//	return 0;
//}
//
//
//
//
//第七题
//#include <stdio.h>
//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);//输出at
//	return 0;
//}
//
//
//
//
//
//第八题 
//#include <stdio.h>
//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);//POINT


//注意这里  *cpp[-2]--> *(cpp-2) 
//	printf("%s\n", *cpp[-2] + 3);//ST
// 
// 
//注意这里cpp[-1][-1] + 1 可以写成 *(*（cpp-1)-1)+1
//	printf("%s\n", cpp[-1][-1] + 1);//EW
//	return 0;
//}