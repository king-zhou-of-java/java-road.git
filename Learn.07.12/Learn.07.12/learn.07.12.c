/*5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：

A选手说：B第二，我第三；

B选手说：我第二，E第四；

C选手说：我第一，D第二；

D选手说：C最后，我第三；

E选手说：我第四，A第一；

比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。*/
//#include<stdio.h>
//int main()
//{
//	int A, B, C, D, E;
//	for (A = 1; A<=5; A++)
//	{
//		for (B = 1;B<=5;B++)
//		{
//			for (C = 1; C<=5; C++)
//			{
//				for (D = 1; D<=5; D++)
//				{
//					for (E = 1; E <= 5; E++)
//					{
//						if ((B == 2) + (A == 3) == 1 &&
//							(B == 2) + (E == 4) == 1 &&
//							(C == 1) + (D == 2) == 1 &&
//							(D == 3) + (C == 5) == 1 &&
//							(E == 4) + (A == 1) == 1 &&
//							A * B * C * D * E == 120)
//						{
//							printf("%d %d %d %d %d", A, B, C, D, E);
//						}
//					}
//				
//				}
//			}
//		}
//	}
//}





/*日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。

以下为4个嫌疑犯的供词:
A说：不是我。

B说：是C。

C说：是D。

D说：C在胡说

已知3个人说了真话，1个人说的是假话。
现在请根据这些信息，写一个程序来确定到底谁是凶手。*/
//#include<stdio.h>
//int main()
//{
//	char killer = 'A';
//	for (killer = 'A'; killer <= 'D'; killer++)
//	{
//		if ((killer != 'A') + (killer == 'C' )+( killer == 'D') + (killer != 'D') == 3)
//		{
//			printf("%c是凶手\n", killer);
//		}
//	}
//	return 0;
//
//}




//打印杨辉三角
//1

//1 1

//1 2 1

//1 3 3 1
#define _CRT_SECURE_NO_WARNINGS

//#include<stdio.h>
//int main()
//{
//	int arr[200][200], i, j, s;
//	printf("请输入你要打印的行数：");
//	scanf("%d", &s);
//	for (i = 0; i <=200; i++)
//	{
//		for (j = 0; j <= 200; j++)
//		{
//			arr[i][j] = 0;
//		}
//	}
//	for (i = 0; i < s; i++)
//	{			
//		arr[i][0] = 1;
//	}
//	for (i = 1; i < s; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			arr[i][j] = arr[i-1][j]+arr[i-1][j-1];
//		}
//	}
//	for (i = 0; i < s; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}




/*实现一个函数，可以左旋字符串中的k个字符。


例如：

即将第一个字符放到字符串的最后 然后整体向前移
ABCD左旋一个字符得到BCDA

ABCD左旋两个字符得到CDAB*/

//正确答案
//#include<stdio.h>
//#include<string.h>
//void left_move(char arr[], int k)
//{
//	int len = strlen(arr);
//	char tmp = arr[0];
//	int i = 0;
//	for (i = 0; i < k; i++)
//	{
//		int j = 0;
//		for (j = 0; j < len-1; j++) 
//		{
//			arr[j] = arr[j + 1];
//		}
//		arr[len- 1] = tmp;
//	}
// int ret  = (strcpy(arr1,arr2);
// if(ret==1)
// {
// return 1;
//}
// else
// return 0;
//int main()
//{
//	int k = 0;
//	char arr[] = "ABCD";
//	printf("请输入你要旋转的次数：");
//	scanf("%d", &k);
//	printf("旋转前：%s\n", arr);
//	left_move(arr, k);
//	printf("旋转后：%s\n", arr);
//	return 0;
//}




//自己写的烂代码
//#include<stdio.h>
//#include<string.h>
//void fun(char arr[],int len)
//{
//	int i = 0;
//	for (i = 0; i < len; i++)
//	{
//		char tmp = arr[0];
//		arr[0] = arr[len - 1];
//		arr[len- 1] = tmp;
//	}
//	printf("%s\n", arr[i]);
//}
//int main()
//{
//	int i = 0;
//	char arr[] = { 0 };
//	printf("请输入你要旋转的字符串");
//	scanf("%s", &arr[i]);
//	int len = strlen(arr);
//	fun(arr, len);
//	return 0;
//}





/*杨氏矩阵    
有一个数字矩阵，
矩阵的每行从左到右是递增的，
矩阵从上到下是递增的，
请编写程序在这样的矩阵中查找某个数字是否存在。*/
//#include <stdio.h>
//int find_num(int arr[3][3], int num, int* px, int* py)
//{
//	//从右上角开始找
//	int x = *px - 1;
//	int y = 0;
//	//与右上角数组进行比较
//	while (x >= 0 && y < *py)
//	{
//		if (num > arr[x][y])
//		{
//			y++;//num比右上角的数字大，往下走一行
//		}
//		else if (num < arr[x][y])
//		{
//			x--;//num比右上角的数字小，往做走一列
//		}
//		else
//		{
//			*px = x;//num的横坐标
//			*py = y;//num的纵坐标
//			return 1;//相等函数值返回1说明已经找到了
//		}
//	}
//	//函数值返回0说明找不到
//	return 0;
//}
//int main()
//{
//	//创建一个二维数组将上图数据存储起来
//	//  1  |  3  |  6
//	//  2  |  5  |  7
//	//  4  |  6  |  8
//	int arr[3][3] = { { 1, 3, 6 },{ 2, 5, 7 },{4, 6, 8} };
//	int x = 3;
//	int y = 3;
//	int num = 5;//要查找的值
//	//用ret接受函数返回值
//	int ret = find_num(arr, num, &x, &y);
//	if (ret == 1)
//	{
//		printf("找得到\n");
//		printf("下标是：%d %d\n", x + 1, y + 1);
//	}
//	else
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}

//练习使用库函数，qsort排序各种类型的数据
//#include<stdio.h>
//
//int cmp1(const void* a, const void* b) {
//	return *(int*)a - *(int*)b;//a＞b，返回正值
//}
//int cmp2(const void* a, const void* b) {
//	return (*(char*)a - *(char*)b);
//}
//int main() {
//	int a[10] = { 2, 3, 1, 2, 3, 4, 5, 6, 7, 2 };
//	qsort(a, 10, sizeof(int), cmp1);
//	for (int i = 0; i < 10; i++) {
//		printf("%d ", a[i]);
//	}
//	printf("\n");
//	char b[10] = { 'd', 'd', 'r', 'r', 't', 'w', 'u', 'i', 'o', 'p' };
//	qsort(b, 10, sizeof(char), cmp2);
//	for (int i = 0; i < 10; i++) {
//		printf("%c ", b[i]);
//	}
//	printf("\n");
//	system("pause");
//	return 0;
//}





//模仿qsort的功能实现一个通用的冒泡排序

//qsore函数使用
//模仿qsort实现一个冒泡排序的通用算法
#include<stdio.h>
#include<stdlib.h>
void Swap(char* buff1, char* buff2, int width);//交换元素的函数声明
//base数组首地址，sz数组元素长度，width元素占多少个字节，
//cmp元素比较的方式的函数，元素相等return 0，大于return 大于零的数，小于则return小于零的数
void bubble_sort(void* base, int sz, int width, int (*cmp)(const void* e1, const void* e2))
{
	int i, j;
	for (i = 0; i < sz; i++)
	{	//一趟的排序
		for (j = 0; j < sz - i - 1; j++)
		{
			//两个元素比较
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				//交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}
int cmp(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}
int main()
{
	int arr[] = { 1,3,4,2,6,5,7,8,9,10 };
	qsort(arr, sizeof(arr) / sizeof(arr[0]), 4, cmp);
	//bubble_sort(arr, sizeof(arr) / sizeof(arr[0]), 4, cmp);

	for (int j = 0; j < 10; j++)
	{
		printf("%d ", arr[j]);
	}

	return 0;
}
void Swap(char* buff1, char* buff2, int width)//交换元素的函数
{
	int i;
	for (i = 0; i < width; i++)
	{
		char tem = *buff1;
		*buff1 = *buff2;
		*buff2 = tem;
		buff1++;
		buff2++;
	}
}

