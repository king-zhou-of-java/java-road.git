#define _CRT_SECURE_NO_WARNINGS

//文件的打开和关闭
//#include <stdio.h>
//int main()
//{
//	FILE* pFile;
//	//打开文件
//	pFile = fopen("nyfie.txt", "w");	
//	//文件操作
//	if (pFile != NULL)
//	{
//		fputs("mianxiangdachangbiancheng", pFile);
//		fclose(pFile);
//	}
//	return 0;
//}


//scanf是针对标准输入的格式化输入语句
//printf是针对标准输出的格式化输入语句
//fscanf是针对所有输入流的格式化输入语句
//fprintf是针对所有输出流的格式化输出语句

//struct S
//{
//	char arr[10];
//	int age;
//	float score;
//};
//int main()
//{
//	struct S s = { "zhangsan",20,50.5f };
//	struct S tmp = { 0 };
//	char buf[100] = { 0 };
//	//sprintf把一个格式化的数据写到字符串中
//	sprintf(buf, "%s %d %f", s.arr, s.age, s.score);
//	//sscanf从一个字符串buf中获取格式化的数据放到tmp中
//	sscanf(buf, "%s %d %f", tmp.arr, &(tmp.age), &(tmp.score));
//	printf("%s %d %f", tmp.arr, tmp.age,tmp.score);
//	printf("%s\n", buf);
//}



//文件的随机读写
//fseek
//int fseek ( FILE * stream, long int offset, int origin );
//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//int main()
//{
//	FILE* pf = fopen("nyfie.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	//读文件
//	//定位文件指针
//	fseek(pf, 2, SEEK_SET);//从初始位置开始读
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//	printf("%d\n", ftell(pf));//ftell定位文件指针 相对于起始位置的偏移量
//	fseek(pf, -1, SEEK_END);//从末尾位置开始读
//	//fseek(pf, 2, SEEK_CUR);//从当前位置开始读
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	rewind(pf);//让文件指针的位置回到文件的起始位置
//	printf("%c\n", ch);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//#include<errno.h>
//#include<string.h>
//#include <stdio.h>
//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("test.txt", "wb");
//	fwrite(&a, 4, 1, pf);//二进制的形式写到文件中
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}



//文件读取结束的判定
// 文本文件的例子
//#include <stdio.h>
//#include <stdlib.h>
//int main(void)
//{
//	int c; // 注意：int，非char，要求处理EOF
//	FILE* fp = fopen("test.txt", "r");
//	if (!fp) {
//		perror("File opening failed");
//		return EXIT_FAILURE;
//	}
//	//fgetc 当读取失败的时候或者遇到文件结束的时候，都会返回EOF
//	while ((c = fgetc(fp)) != EOF) // 标准C I/O读取文件循环
//	{
//		putchar(c);
//	}
//		//判断是什么原因结束的
//		if (ferror(fp))
//			puts("I/O error when reading");
//		else if (feof(fp))
//			puts("End of file reached successfully");
//	fclose(fp);
//}



//二进制文件的例子：
//#include <stdio.h>
//enum { SIZE = 5 };
//int main(void)
//{
//	double a[SIZE] = { 1.,2.,3.,4.,5. };
//	FILE* fp = fopen("test.bin", "wb"); // 必须用二进制模式
//	fwrite(a, sizeof * a, SIZE, fp); // 写 double 的数组
//	fclose(fp);
//	double b[SIZE];
//	fp = fopen("test.bin", "rb");
//	size_t ret_code = fread(b, sizeof * b, SIZE, fp); // 读 double 的数组
//	if (ret_code == SIZE) {
//		puts("Array read successfully, contents: ");
//		for (int n = 0; n < SIZE; ++n) printf("%f ", b[n]);
//		putchar('\n');
//	}
//	else { // error handling
//		if (feof(fp))
//			printf("Error reading test.bin: unexpected end of file\n");
//		else if (ferror(fp)) {
//			perror("Error reading test.bin");
//		}
//	}
//	fclose(fp);
//}




//文件缓冲区
#include <stdio.h>
#include <windows.h>
//VS2013 WIN10环境测试
int main()
{
	FILE* pf = fopen("test.txt", "w");
	fputs("abcdef", pf);//先将代码放在输出缓冲区
	printf("睡眠10秒-已经写数据了，打开test.txt文件，发现文件没有内容\n");
	Sleep(10000);
	printf("刷新缓冲区\n");
	fflush(pf);//刷新缓冲区时，才将输出缓冲区的数据写到文件（磁盘）
	//注：fflush 在高版本的VS上不能使用了
	printf("再睡眠10秒-此时，再次打开test.txt文件，文件有内容了\n");
	Sleep(10000);
	fclose(pf);
	//注：fclose在关闭文件的时候，也会刷新缓冲区
	pf = NULL;
	return 0;
}