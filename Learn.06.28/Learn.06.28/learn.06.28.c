#include <stdio.h>
//360面试题
int main()
{
 	int i = 0, a = 0, b = 2, c = 3, d = 4;
	//i = a++ && ++b && d++;
	//a为0的话就不用计算后面的表达式了
	//a若为1后面的表达式还要计算
	i = a++||++b||d++;
	//或操作有一个为真  后面的表达式就不用算了
	printf("a = %d\n b = %d\n c = %d\nd = %d\n", a, b, c, d);
	return 0;
}