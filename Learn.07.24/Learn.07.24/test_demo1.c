#include "DynamicContact.h"

void menu()
{
	printf("================================\n");
	printf("*********** Contact ************\n");
	printf("================================\n");
	printf("***     1.add     2.del      ***\n");
	printf("***     3.search  4.modify   ***\n");
	printf("***     5.sort    6.print    ***\n");
	printf("***     0.exit               ***\n");
	printf("================================\n");

}
void test()
{
	int intput = 0;
	Contact con;//创建通讯录
	InitContact(&con);//初始化通讯录
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &intput);
		switch (intput)
		{
		case ADD:
			AddContact(&con);
			break;
		case DEL:
			DelContact(&con);
			break;
		case SEARCH:
			SearchContact(&con);
			break;
		case MODIFY:
			ModifyContact(&con);
			break;
		case SORT:
			SortContact(&con);
			break;
		case PRINT:
			PrintContact(&con);
			break;
		case EXIT:
			SaveContact(&con);//销毁通讯录之前把数据存入文件中
			DestroyContact(&con);
			printf("退出通讯录\n");
			break;
		default:
			printf("输入错误，请重新输入\n\n");
			break;
		}

	} while (intput);
}
int main()
{
	test();
	return 0;
}