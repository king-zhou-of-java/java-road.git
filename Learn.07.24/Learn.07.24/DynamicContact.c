#include "DynamicContact.h"

//排序所用菜单
void menu2()
{
	printf("********************************\n");
	printf("******  1.name    2.age   ******\n");
	printf("******  0.exit            ******\n");
	printf("********************************\n");
}
//修改联系人所用的菜单
void menu1()
{
	printf("********************************\n");
	printf("******  1.name    2.sex   ******\n");
	printf("******  3.age     4.tele  ******\n");
	printf("******  5.addr    0.exit  ******\n");
	printf("********************************\n");

}

//检测通讯录容量
void CheckCapacity(Contact* p)
{
	assert(p);
	if (p->capacity == p->count)
	{
		PeoInfo* tmp = (PeoInfo*)realloc(p->data, (p->capacity + 2) * sizeof(PeoInfo));
		if (p->data != NULL)
		{
			p->data = tmp;
		}
		else
		{
			perror("CheckCapacity::realloc");
			return;
		}

		p->capacity += 2;
		printf("增容成功\n");
	}
}

//初始化通讯录
void InitContact(Contact* p)
{
	assert(p);
	p->count = 0;
	p->capacity = DEFAULT_SZ;
	p->data = (PeoInfo*)malloc(p->capacity * sizeof(PeoInfo));

	if (p->data == NULL)
	{
		perror("InitContact::malloc");
		return;
	}

	memset(p->data, 0, p->capacity * sizeof(PeoInfo));//把PeoInit全部初始化为0

	//加载文件信息到通讯录中
	LoadContact(p);
}

//销毁通讯录
void DestroyContact(Contact* p)
{
	free(p->data);
	p->data = NULL;
	p->capacity = 0;
	p->count = 0;

	printf("销毁成功\n");
}

//添加联系人
void AddContact(Contact* p)
{
	//检查容量
	CheckCapacity(p);

	//录入信息
	printf("请输入名字:>");
	scanf("%s", p->data[p->count].name);
	printf("请输入性别:>");
	scanf("%s", p->data[p->count].sex);
	printf("请输入年龄:>");
	scanf("%d", &(p->data[p->count].age));
	printf("请输入电话:>");
	scanf("%s", p->data[p->count].tele);
	printf("请输入地址:>");
	scanf("%s", p->data[p->count].addr);

	p->count++;
	printf("添加成功\n\n");
}

//查找，找到了返回下标，找不到返回 -1
int FindName(const Contact* p, char name[])
{
	assert(p);
	int i = 0;
	for (i = 0; i < p->count; i++)
	{
		if (0 == strcmp(p->data[i].name, name))
		{
			return i;
		}
	}
	return -1;
}

//删除联系人
void DelContact(Contact* p)
{
	assert(p);
	if (0 == p->count)
	{
		printf("通讯录已空，无法删除\n");
		return;
	}
	char name[NAME_MAX];
	printf("请输入要查找的名字:>");
	scanf("%s", name);
	int position = FindName(p, name);//查找
	if (-1 == position)
	{
		printf("要删除的联系人不存在\n\n");
		return;
	}
	//删除
	int i = 0;
	for (i = position; i < p->count - 1; i++)
	{
		p->data[i] = p->data[i + 1];
	}
	p->count--;
	printf("删除成功\n\n");
}

//查找联系人
void SearchContact(const Contact* p)
{
	assert(p);
	char name[NAME_MAX];
	printf("请输入要查找的名字:>");
	scanf("%s", name);
	int position = FindName(p, name);//查找
	if (-1 == position)
	{
		printf("要查找的联系人不存在\n\n");
		return;
	}
	printf("\n-----------------------------------------------\n");
	printf("%-10s %-5s %-5s %-12s %-30s\n", "姓名", "性别", "年龄", "电话", "地址");
	printf("%-10s %-5s %-5d %-12s %-30s\n", p->data[position].name,
		p->data[position].sex, p->data[position].age,
		p->data[position].tele, p->data[position].addr);
	printf("\n-----------------------------------------------\n\n");
}

//修改联系人信息
void ModifyContact(Contact* p)
{
	assert(p);
	int intput = 0;
	char name[NAME_MAX];
	printf("请输入要修改联系人的名字:>");
	scanf("%s", name);
	int position = FindName(p, name);//查找
	if (-1 != position)
	{
		printf("\n-----------------------------------------------\n");
		printf("%-10s %-5s %-5s %-12s %-30s\n", "姓名", "性别", "年龄", "电话", "地址");
		printf("%-10s %-5s %-5d %-12s %-30s\n", p->data[position].name, p->data[position].sex,
			p->data[position].age, p->data[position].tele, p->data[position].addr);
		printf("\n-----------------------------------------------\n\n");
		do
		{
			menu1();
			printf("请输入要修改的选项:>");
			scanf("%d", &intput);
			switch (intput)
			{
			case NAME:
				printf("请修改名字:>");
				scanf("%s", p->data[position].name);
				printf("修改成功\n\n");
				break;
			case SEX:
				printf("请修改性别:>");
				scanf("%s", p->data[position].sex);
				printf("修改成功\n\n");
				break;
			case AGE:
				printf("请修改年龄:>");
				scanf("%d", &(p->data[position].age));
				printf("修改成功\n\n");
				break;
			case TELE:
				printf("请修改电话号码:>");
				scanf("%s", p->data[position].tele);
				printf("修改成功\n\n");
				break;
			case ADDR:
				printf("请修改地址:>");
				scanf("%s", p->data[position].addr);
				printf("修改成功\n\n");
				break;
			case EXIT0:
				printf("退出修改\n\n");
				break;
			default:
				printf("选择错误，请重新选择\n\n");
				break;
			}
		} while (intput);
	}
	else
	{
		printf("所要修改的联系人不存在\n\n");
		return;
	}
}

//打印联系人
void PrintContact(const Contact* p)
{
	assert(p);
	int i = 0;
	printf("\n-----------------------------------------------\n");
	printf("%-10s %-5s %-5s %-12s %-30s\n", "姓名", "性别", "年龄", "电话", "地址");
	for (i = 0; i < p->count; i++)
	{
		printf("%-10s %-5s %-5d %-12s %-30s\n", p->data[i].name, p->data[i].sex,
			p->data[i].age, p->data[i].tele, p->data[i].addr);
	}
	printf("-----------------------------------------------\n\n");
}


int cmp_name(const void* e1, const void* e2)
{
	return strcmp(((struct PeoInfo*)e1)->name, ((struct PeoInfo*)e2)->name);
}

int cmp_age(const void* e1, const void* e2)
{
	return (((struct PeoInfo*)e1)->age - ((struct PeoInfo*)e2)->age);
}

//排序
void SortContact(const Contact* p)
{
	assert(p);
	int intput = 0;
	do
	{
		menu2();
		printf("请选择需要排序的选项:>");
		scanf("%d", &intput);
		switch (intput)
		{
		case 1:
			qsort(p->data, p->count, sizeof(struct PeoInfo), cmp_name);
			printf("按名字排序成功\n\n");
			break;
		case 2:
			qsort(p->data, p->count, sizeof(struct PeoInfo), cmp_age);
			printf("按年龄排序成功\n\n");
			break;
		case 0:
			printf("退出排序\n\n");
			break;
		default:
			printf("选择错误，请重新选择\n\n");
			break;
		}
	} while (intput);

}


//保存通讯录的信息到文件
void SaveContact(const Contact* p)
{
	//打开并创建文件
	FILE* pf = fopen("contact.data.txt", "w");//w：只写，
	if (pf == NULL)
	{
		perror("SaveContact::fopen");
		return;
	}

	//写文件
	int i = 0;
	for (i = 0; i < p->count; i++)
	{
		fwrite(p->data + i, sizeof(PeoInfo), 1, pf);
	}

	//关闭文件
	fclose(pf);
	pf = NULL;
}

//加载文件信息到通讯录中
void LoadContact(Contact* p)
{
	//打开文件
	FILE* pf = fopen("contact.data.txt", "r");//r:只读
	if (pf == NULL)
	{
		perror("LoadContact::fopen");
		return;
	}


	//读文件
	PeoInfo tmp = { 0 };
	while (fread(&tmp, sizeof(PeoInfo), 1, pf))
	{
		CheckCapacity(p);
		p->data[p->count] = tmp;
		p->count++;
	}

	//关闭文件
	fclose(pf);
	pf = NULL;

}
