#pragma once
#pragma once
#define _CRT_SECURE_NO_WARNINGS //vs 编译器需要，其他编译器不需要，可自行删去

//动态版通讯录

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <Windows.h>

//类型声明

//PeoInit结构体所用
#define NAME_MAX 20
#define SEX_MAX 5
#define TELE_MAX 12
#define ADDR_MAX 30
//通讯录初始状态的容量大小
#define DEFAULT_SZ 3

//枚举选项

enum Option //test函数所用的枚举
{
	EXIT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};

enum Modify //修改联系人所用的枚举
{
	EXIT0,
	NAME,
	SEX,
	AGE,
	TELE,
	ADDR
};

//结构体声明

typedef struct PeoInfo
{
	char name[NAME_MAX];
	char sex[SEX_MAX];
	int age;
	char tele[TELE_MAX];
	char addr[ADDR_MAX];
}PeoInfo;

typedef struct Contact
{
	PeoInfo* data;//存放联系人的信息
	int count;//通讯录中已经保存的信息个数
	int capacity;//记录通讯录当前的最大容量
}Contact;


//函数声明

//初始化通讯录
void InitContact(Contact* p);

//销毁通讯录
void DestroyContact(Contact* p);

//添加联系人
void AddContact(Contact* p);

//删除联系人
void DelContact(Contact* p);

//查找联系人
void SearchContact(const Contact* p);

//修改联系人信息
void ModifyContact(Contact* p);

//打印联系人
void PrintContact(const Contact* p);

//排序
void SortContact(const Contact* p);

//保存通讯录的信息到文件
void SaveContact(const Contact* pc);

//加载文件信息到通讯录中
void LoadContact(Contact* p);