//#include <stdio.h>
//int main()
//{
//	int a = 0, b = 0;
//	for (a = 1, b = 1; a <= 100; a++)
//	{
//		if (b >= 20) break;
//		if (b % 3 == 1)
//		{
//			b = b + 3;
//			continue;
//		}
//		else
//		    b = b - 5;
//	}
//	printf("%d\n", a);
//	return 0;
//}
//#include<stdio.h>
// //计算1-100之间有多少个数字出现9
//int main()
//{
//	int sum = 0;
//	for (int i = 9; i <= 100; i++)
//	{
//		i += 10;
//		sum += 1;
//	}
//	printf("1到100之间共有%d个数字出现数字9", sum);
//	return 0;
//}
//#include<stdio.h>
////计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
//int main()
//{
//	float sum = 0.0;
//	for (int i = 1; i <= 100; i++)
//	{
//		if (i % 2 == 0)
//		{
//			sum = sum - (1.0 / i);
//		}
//		else
//			sum = sum + (1.0 / i);
//	}
//	printf("%f", sum);
//	return 0;
//}
//#include<stdio.h>
////求10个整数中的最大值
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int max = arr[0];
//	for (int i = 0; i < sz; i++)
//	{
//		if (arr[i] > max)
//			max = arr[i];
//	}
//	printf("%d", max);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int x, y;
//    int sum = 1;
//
//    for (x = 1; x <= 9; x++) {
//        for (y = 1; y <= x; y++) {
//            sum = x * y;
//            printf("%d*%d=%d  ", y, x, sum);
//            if (sum < 10)
//            {
//                printf(" ");
//            }
//        }
//        printf("\n");
//
//    }
//    return 0;
//}
//猜数字游戏
//#define _CRT_SECURE_NO_WARNINGS 1 
//#include <stdio.h>
//#include <stdlib.h>
//#include <time.h>
//void menu()
//{
//	printf("**********************************\n");
//	printf("*********** 1.play     **********\n");
//	printf("*********** 0.exit     **********\n");
//	printf("**********************************\n");
//}
////RAND_MAX--rand函数能返回随机数的最大值。
//void game()
//{
//	int random_num = rand() % 100 + 1;
//	int input = 0;
//	while (1)
//	{
//		printf("请输入猜的数字>:");
//		scanf("%d", &input);
//		if (input > random_num)
//		{
//			printf("猜大了\n");
//		}
//		else if (input < random_num)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择>:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			break;
//		default:
//			printf("选择错误,请重新输入!\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//#define _CRT_SECURE_NO_WARNINGS 1 
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int  k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz;
//	int mid = (left + right - 1) / 2;
//	while(left<=right)
//	{
//		if (arr[mid] < k)
//			left = mid + 1;
//		else if (arr[mid] > k)
//			right = mid - 1;
//		else
//			printf("找到了,下标为%d\n",mid);
//		break;
//	}
//	if (left > right)
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}
//二分查找
//#define _CRT_SECURE_NO_WARNINGS 
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//
//	while (left <= right)
//	{
//		//int mid = (left + right) / 2;
//		int mid = left + (right - left) / 2;//防止数据过大，越界
//
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了，下标是:%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//		printf("找不到\n");
//
//	return 0;
//}
//#include<stdio.h>
//void swap(int *x, int *y)
//{
//	int temp = *x;
//	*x = *y;
//	*y = temp;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("交换前 a=%d b = %d\n", a, b);
//	swap(&a,&b);
//	printf("交换后 a=%d b = %d",a, b);
//	return 0;
//}
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//自己想打印多少行的乘法表就打印多少行
int main()
{
	int k = 0;
	int i = 0;
	printf("请输入你要打印多少行的乘法表：");
	scanf("%d", &k);
	for (int i = 1; i <= k; i++)
	{
		int j = 0;
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%2d ", i, j, i * j);
		}
		printf("\n");
	}
	return 0;
}