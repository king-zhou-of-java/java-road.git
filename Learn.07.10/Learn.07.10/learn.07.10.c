//#include<stdio.h>
//#include<string.h>
//#include<stdlib.h>
//void fun()
//{
//
//}
//int main()
//{
//
//}
//void qsort(void* base,//排序数据的起始位置
//	size_t num,//待排序的数据元素的个数
//	size_t width,//待排序数据元素的大小
//	int(*cmp)(const void* e1, const void* e2));//比较函数
//void swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//		char tmp = *buf1;
//	    *buf1 = *buf2;
//	   *buf2 = tmp;
//	   int arr[10];//指向数组的指针就叫数组指针
//	   int(*Parr)[10] = &arr;
//	 
//
//}
////函数的地址存放在函数指针中
//int Add(int x, int y)
//{
//	return x + y;
//}
//int (*pf)(int, int) = &Add;
//int sum = pf(2, 3);
//printf("%d", sum);
//int (*parr[4])();//函数指针数组
#include<stdio.h>
#include<string.h>
int main()
{



//	//一维数组
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));//16 
//
//
//	//4 a不是单独放在sizeof内部 也没有单独取地址 
//	//所以a就是首元素的地址 a+0还是首元素的地址
//	printf("%d\n", sizeof(a + 0));
//
//
//	//a<-->&a[0]    *a中的a是首元素的地址
//	printf("%d\n", sizeof(*a));//4
//
//
////这里的数组名表示首元素的地址 a+1 表示跳过一个元素 表示a[1]
//	printf("%d\n", sizeof(a + 1));//4
//
//
//	printf("%d\n", sizeof(a[1]));//4
//
//
//	//&a取出的是数组的地址 也就是个地址 
//	printf("%d\n", sizeof(&a));//4
//
//	//两种理解！
//	//&a得到整个数组的地址 然后*解引用访问整个数组
//	//& * 相互抵消 一个取地址 一个解引用 最后就是a 
//	printf("%d\n", sizeof(*&a));//16
//
//
//	//&a取出的是数组的地址
//	//&a--> int(*)[4]
//	//&a+1 是从数组a的地址向后跳过了一个数组的大小 
//	//即使是跳过了一个数组的大小 但是地址还是地址 就是4/8个字节
//	printf("%d\n", sizeof(&a + 1));//4
//
//
//	printf("%d\n", sizeof(&a[0]));//   4/8
//
//
//printf("%d\n", sizeof(&a[0] + 1));//这里是第二个元素的地址  4/8








        //字符数组
//char arr[] = { 'a','b','c','d','e','f' };
//
//printf("%d\n", sizeof(arr));//6
//
////arr+0是数组首元素的地址
//printf("%d\n", sizeof(arr + 0));//   4/8
//
//
////*arr就是数组首元素，大小是一个字节
//printf("%d\n", sizeof(*arr));//1
//
//
//printf("%d\n", sizeof(arr[1]));//1
//
//
////&arr是数组的地址 是地址就是4/8个字节
//printf("%d\n", sizeof(&arr));//4
//
//
//
//printf("%d\n", sizeof(&arr + 1));//4
//
//
//
////&arr[0] + 1是第二个元素的地址 是地址大小就是4/8个字节
//printf("%d\n", sizeof(&arr[0] + 1));//4
//
//
////strlen直到找到'\0'的位置才停下来 ，下面两个值是一样的
//printf("%d\n", strlen(arr));//随机值
//printf("%d\n", strlen(arr + 0));//随机值
//
//
////strlen('a')对应的ASCII值-->strlen(97);把97当做一个野指针来访问 非法
//printf("%d\n", strlen(*arr));
//
//
////arr[1]相当于传过来一个'b' 对应的ASCII值为98 跟上面类似
//printf("%d\n", strlen(arr[1]));
//
//
//printf("%d\n", strlen(&arr));//随机值
//
//printf("%d\n", strlen(&arr + 1));//随机值-6
//
//printf("%d\n", strlen(&arr[0] + 1));//随机值-1







//char arr[] = "abcdef";
//printf("%d\n", sizeof(arr));//   7    a   b c d e f \0
//printf("%d\n", sizeof(arr + 0));//    4/8
//printf("%d\n", sizeof(*arr));//1
//printf("%d\n", sizeof(arr[1]));//1
//printf("%d\n", sizeof(&arr));//  整个数组的地址  4/8
//printf("%d\n", sizeof(&arr + 1));//    4/8
//printf("%d\n", sizeof(&arr[0] + 1));//   4/8
//
//
////strlen计算的是\0之前出现的字符个数
////sizeof只关注占用内存空间的大小，不在乎内存中放的什么
//printf("%d\n", strlen(arr));//6
//printf("%d\n", strlen(arr + 0));//6
//printf("%d\n", strlen(*arr));//err
//printf("%d\n", strlen(arr[1]));//err
//printf("%d\n", strlen(&arr));//6
//printf("%d\n", strlen(&arr + 1));//随机值
//printf("%d\n", strlen(&arr[0] + 1));//5
//
//
//char* p = "abcdef";
//printf("%d\n", sizeof(p));// p是一个指针  4 / 8
//printf("%d\n", sizeof(p + 1));// 4/8
//printf("%d\n", sizeof(*p));//   1   p是一个指针变量 对p解引用访问的是a 一个字节
//printf("%d\n", sizeof(p[0]));// 1   p[0]-->*(p+0)-->*p
//printf("%d\n", sizeof(&p));//可以理解为二级指针 指针大小就是 4/8
//printf("%d\n", sizeof(&p + 1));//+1还是指针 大小为4/8
//
//
////p[0]相当于首字符a，取出a的地址+1，那就是b的地址  地址大小就是4/8个字节
//printf("%d\n", sizeof(&p[0] + 1)); 
//
//printf("%d\n", strlen(p));//6
//printf("%d\n", strlen(p + 1));//5
//printf("%d\n", strlen(*p));//err
//printf("%d\n", strlen(p[0]));//err
//printf("%d\n", strlen(&p));//随机值 从a的地址开始找 不知道啥时候会遇到\0
//printf("%d\n", strlen(&p + 1));//随机值 即跳过p的地址  道理和上面一样
//
////5     p[0]就是a,取出a的地址然后+1，得到b的地址，往后数就是五个字符
//printf("%d\n", strlen(&p[0] + 1));



int a[3][4] = { 0 };
printf("%d\n", sizeof(a));//48
printf("%d\n", sizeof(a[0][0]));// 4


//a[0]想象成第一行的数组名    数组名单独存放在sizeof内部
//一行有四列 即数组a[0]里面有四个元素  所以大小4*4=16
printf("%d\n", sizeof(a[0]));//16


//这里的a[0]相当于&a[0][0]+1 即第一行第二个元素的地址 是地址大小为4/8字节
printf("%d\n", sizeof(a[0] + 1));//4


//参考上面 因为（a[0]+1）相当于第二个元素的地址 所以*(a[0]+1)就相当于第二个元素 大小为4个字节
printf("%d\n", sizeof(*(a[0] + 1)));//4


//a表示首元素的地址 二维数组首元素就是第一行的元素 a+1表示跳过第一行 即第二行的地址
printf("%d\n", sizeof(a + 1));//  4/8


//对第二行的地址解引用 相当于拿到了第二行 计算的是第二行的大小
// sizeof(*(a + 1))相当于sizeof（a[1])
printf("%d\n", sizeof(*(a + 1)));//  16


//&a[0]相当于第一行的地址 +1  即得到的是第二行的地址
printf("%d\n", sizeof(&a[0] + 1));//4

//即对第二行的地址解引用得到的整个第二行
printf("%d\n", sizeof(*(&a[0] + 1)));//16

//a表示首元素的地址 也就是第一行的地址 对第一行的地址解引用 拿到的就是一行
printf("%d\n", sizeof(*a));//16


printf("%d\n", sizeof(a[3]));//16

}