//实现数组元素的逆置
//void Reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
//#include<stdio.h>
////冒泡排序
//void fun(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		for (int j = 0; j < sz - i - 1; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 12,66,34,24,55,67,46,79,78,90 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	fun(arr,sz);
//	for (int a = 0; a < sz; a++)
//	{
//		printf("%d ", arr[a]);
//	}
//	return 0;
//}
// 
// 
// 
// 
// 
//#define _CRT_SECURE_NO_WARNINGS
//
//#include "game.h"
//void menu()
//{
//	printf("*******************\n");
//	printf("******1.play*******\n");
//	printf("******0.exit*******\n");
//}
//void game()
//{
//	char ret = 0;
//	char board[ROW][COL] = {0};
//	InitBoard(board, ROW,COL);
//	DisplayBoard(board, ROW, COL);
//	while (1)//下棋
//	{
//		PlayerMove(board, ROW, COL);//玩家下棋
//		DisplayBoard(board, ROW, COL);
//		//判断输赢
//		ret = IsWin(board,ROW,COL);
//		if (ret != 'C')
//		{
//			break;
//		}
//		ComputerMove(board, ROW, COL);
//		DisplayBoard(board, ROW, COL);
//		//判断输赢
//		ret = IsWin(board, ROW, COL);
//		if (ret != 'C')
//		{
//			break;
//		}
//		if (ret == '*')
//		{
//			printf("玩家赢了，游戏退出\n");
//		}
//		else if (ret == '#')
//		{
//			printf("电脑赢了，游戏退出\n");
//		}
//		else
//			printf("平局，游戏退出\n");
//	}
//		DisplayBoard(board, ROW, COL);
//}
//void test()
//{
//	srand((unsigned int)time(NULL));//设置随机数的生成起点
//	int input = 0;
//	do
//	{
//		menu();//打印菜单
//		printf("请选择：>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏！\n");
//			break;
//		default:
//			printf("选择错误：");
//			break;
//		}
//	} while (input);
//}
//int main()
//{
//	test();
//	return 0;
//}
//这里是代码的整体逻辑 文件名为test.c
//#define _CRT_SECURE_NO_WARNINGS
//#include"game.h"
//void meat()
//{
//    printf("---------------------\n");
//    printf("-----1.开始游戏------\n");
//    printf("-----2.退出游戏------\n");
//    printf("---------------------\n");
//    printf("---------------------\n");
//}
//void game()
//{
//    char ret = 0;
//    char board[ROW][COL] = { 0 };
//    InitBoard(board, ROW, COL);
//    dispalyBoard(board, ROW, COL);
//    while (1)
//    {
//        playerMove(board, ROW, COL);//玩家下棋
//        ret = Iswin(board, ROW, COL);//判断输赢
//        if (ret != 'c')
//        {
//            break;
//        }
//        dispalyBoard(board, ROW, COL);//打印棋盘
//        ComputerMoce(board, ROW, COL);//电脑下棋
//        ret = Iswin(board, ROW, COL);//判断输赢
//        if (ret != 'c')
//        {
//            break;
//        }
//        dispalyBoard(board, ROW, COL);//打印棋盘
//    }
//
//    if (ret == 'x')
//    {
//        printf("玩家赢了！\n");
//    }
//    else if (ret == 'o')
//    {
//        printf("电脑赢了！\n");
//    }
//    else
//    {
//        printf("平局\n");
//    }
//    dispalyBoard(board, ROW, COL);
//
//}
//
//int main()
//{
//    srand((unsigned int)time(NULL));
//    int input = 0;
//    do {
//        meat();
//        printf("请选择>:");
//        scanf("%d", &input);
//        if (input == 1)
//        {
//            game();
//        }
//        else if (input == 0)
//        {
//            printf("退出游戏\n");
//        }
//        else
//        {
//            printf("请重新选择\n");
//        }
//
//    } while (input);
//
//    return 0;
//}
#define _CRT_SECURE_NO_WARNINGS 1

//测试三子棋游戏的逻辑

#include "game.h"

void menu()
{
	printf("********************************\n");
	printf("*********  1. play     *********\n");
	printf("*********  0. exit     *********\n");
	printf("********************************\n");
}

void game()
{
	char ret = 0;
	//存放下棋的数据
	char board[ROW][COL] = { 0 };
	//初始化棋盘为全空格
	InitBoard(board, ROW, COL);
	//打印棋盘
	DisplayBoard(board, ROW, COL);
	while (1)
	{
		//玩家下棋
		player_move(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		//判断输赢
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		//电脑下棋
		computer_move(board, ROW, COL);//随机下棋
		DisplayBoard(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
	}
	if (ret == '*')
	{
		printf("玩家赢了\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢了\n");
	}
	else
	{
		printf("平局\n");
	}
	//DisplayBoard(board, ROW, COL);
}

//
//什么情况，游戏就结束了
//玩家赢 - '*'
//电脑赢 - '#'
//平局   - 'Q'
//继续   - 'C'
//

void test()
{
	int input = 0;
	srand((unsigned int)time(NULL));

	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();//游戏
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);
}

int main()
{
	test();
	return 0;
}