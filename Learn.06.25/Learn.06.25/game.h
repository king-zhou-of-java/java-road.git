//#pragma once
//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//#define ROW 3
//#define COL 3
////初始化棋盘
//void InitBoard(char board[ROW][COL], int row, int col);
////打印棋盘
//void DisplayBoard(char board[ROW][COL], int row, int col);
////玩家下棋
//void PlayerMove(char board[ROW][COL], int row, int col);
////电脑下棋
//void ComputerMove(char board[ROW][COL], int row, int col);
////玩家赢 返回*
////电脑赢 返回#
////平局 - Q
////继续 - C
//char IsWin(char board[ROW][COL],int row,int col);
//这里是定义的头文件以及声明 头文件名game.h
//#pragma once
//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//#define ROW 3
//#define COL 3
////初始化棋盘
//InitBoard(char board[ROW][COL], int row, int col);
////打印棋盘
//dispalyBoard(char board[ROW][COL], int row, int col);
////玩家下棋
//void playerMove(char board[ROW][COL], int row, int col);
//电脑下棋
//void ComputerMoce(char board[ROW][COL], int row, int col);
////判断输赢
//char Iswin(char board[ROW][COL], int row, int col);
#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW 3
#define COL 3

//初始化棋盘
void InitBoard(char board[ROW][COL], int row, int col);

//打印棋盘
void DisplayBoard(char board[ROW][COL], int row, int col);

//玩家下棋
void player_move(char board[ROW][COL], int row, int col);

//电脑下棋
void computer_move(char board[ROW][COL], int row, int col);

//判断输赢
char is_win(char board[ROW][COL], int row, int col);