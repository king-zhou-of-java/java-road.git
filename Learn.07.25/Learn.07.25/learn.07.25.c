﻿#include<stdio.h>
//struct Stu
//{
//	char name[20];
//	int age;
//}s1,s2;
////匿名结构体类型 只能用一次
//struct
//{
//
//}s1;
//
////结构体的自引用
////代码2
//struct Node
//{
//	int data;
//	struct Node* next;
//};




//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
////初始化：定义变量的同时赋初值。
//struct Point p3 = { x, y };
//struct Stu     //类型声明
//{
//	char name[15];//名字
//	int age;    //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化
//int main()
//{
//	struct Point s1 = { "zhangsan",20 };
//}
//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//}




//enum Day//星期
//{
//	Mon,
//	Tues,
//	Wed,
//	Thur,
//	Fri,
//	Sat,
//	Sun
//};
//enum Sex//性别
//{
//	MALE,
//	FEMALE,
//	SECRET
//}；
//enum Color//颜色
//{
//	RED,
//	GREEN,
//	BLUE
//};
//
//int main()
//{
//	//这些可能取值都是有值的，默认从0开始，一次递增1，当然在定义的时候也可以赋初值
//	printf("%d\n", RED);
//	printf("%d\n", GREEN);
//	printf("%d\n", BLUE);
//	enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异
//}


union Un
{
	int i;
	char c;
};
union Un un;
// 下面输出的结果是一样的吗？
int main()
{
	printf("%d\n", &(un.i));
	printf("%d\n", &(un.c));
	/*联合的成员是共用同一块内存空间的，
	这样一个联合变量的大小，至少是最大成员的大小（因为联
	合至少得有能力保存最大的那个成员）。*/
}