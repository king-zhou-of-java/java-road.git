//#include <stdio.h>
//出生日期输入输出
//int main() {
//
//    int year = 0, month = 0, day = 0;
//    scanf("%4d%2d%2d", &year, &month, &day);
//    //使用%0可以填充前导0
//    printf("year=%d\n", year);
//    printf("month=%02d\n", month);
//    printf("date=%02d\n", day);
//    return 0;
//}
//求两个数的最大公约数
//int main()
//{
//	int a = 18;
//	int b = 24;
//	int c = 0;
//
//	while (c = a % b)
//	{
//		a = b;
//		b = c;
//	}
//
//	printf("%d\n", b);
//	return 0;
//}
#define _CRT_SECURE_NO_WARNINGS
//求两个数最小公倍数
//#include<stdio.h>
//int main()
//{
//	int m, n, temp, i;
//	printf("Input m & n:");
//	scanf("%d%d", &m, &n);
//	if (m < n) // 比较大小，使得m中存储大数，n中存储小数 
//	{
//		temp = m;
//		m = n;
//		n = temp;
//	}
//	for (i = m; i > 0; i++) // 从大数开始寻找满足条件的自然数 
//		if (i % m==0 && i % n==0)
//		{
//			// 输出满足条件的自然数并结束循环 
//				printf("The LCW of % d and %d is : % d\n", m, n, i);
//			break;
//		}
//	return 0;
//}
//打印100-200之间的素数
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 101; i <= 200; i += 2)
//	{
//		//判断i是否为素数
//		//2->i-1
//		int j = 0;
//		for (j = 2; j <= sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//		}
//		if (j > sqrt(i))
//		{
//			count++;
//			printf("%d ", i);
//		}
//	}
//	printf("\ncount = %d\n", count);
//	return 0;
//}
//编写程序数一下 1到 100 的所有整数中出现多少个数字9
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int count = 0;
//
//
//	for (i = 1; i <= 100; i++)
//	{
//		if (i % 10 == 9)
//			count++;
//		if (i / 10 == 9)
//			count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}
////求10 个整数中最大值
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int max = 0;
//
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	//
//	max = arr[0];
//	for (i = 1; i < 10; i++)
//	{
//		if (arr[i] > max)
//			max = arr[i];
//	}
//	printf("max = %d\n", max);
//	return 0;
//}
//在屏幕上输出9 * 9乘法口诀表
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	//控制行数
//	for (i = 1; i <= 9; i++)
//	{
//		//打印每一行内容，每行有i个表达式
//		int j = 0;
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//编写代码在一个整形有序数组中查找具体的某个数

//要求：找到了就打印数字所在的下标，找不到则输出：找不到。
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	while (left <= right)
//	{
//		//int mid = (left + right) / 2;
//		int mid = left + (right - left) / 2;//防止数据过大，越界
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了，下标是:%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//		printf("找不到\n");
//	return 0;
//}
//交换两个数组中的内容
//#include <stdio.h>
//int main()
//{
//	int arr1[10] = { 0 };
//	int arr2[10] = { 0 };
//	int i = 0;
//	printf("请输入第一个数组中的内容>");
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	printf("请输入10个数字:>");
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//	//交换
//	for (i = 0; i < 10; i++)
//	{
//		int tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//	return 0;
//}
/*创建一个整形数组，完成对数组的操作

实现函数init() 初始化数组为全0
实现print()  打印数组的每个元素
实现reverse()  函数完成数组元素的逆置。
要求：自己设计以上函数的参数，返回值。*/
//void Init(int arr[], int sz, int set)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = set;
//	}
//}
//
//
//void Print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//
//void Reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//
//
//	while (left < right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//}
/*输入描述：-
第一行输入一个整数(0≤N≤50)。

第二行输入N个整数，输入用空格分隔的N个整数。

第三行输入想要进行删除的一个整数。

输出描述：
输出为一行，删除指定数字之后的序列。*/
//#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//	int arr[50] = { 0 };
//	int n;
//	scanf("%d", &n); //行数
//	for (int i = 0; i < n; i++) {
//		scanf("%d", &arr[i]); //输入序列
//	}
//	int d; //要删的数字
//	scanf("%d", &d);
//	for (int i = 0; i < n; i++)//将要删的数字赋值为0
//	{
//		if (arr[i] == d)
//			arr[i] = 0;
//	}
//	for (int i = 0; i < n; i++)
//	{
//		if (arr[i])
//			printf("%d ", arr[i]); //打印非0的数字
//	}
//	return 0;
//}
/*输入描述：
两行，第一行为n，表示n个成绩，不会大于10000。

第二行为n个成绩（整数表示，范围0~100），以空格隔开。
输出描述：
一行，输出n个成绩中最高分数和最低分数的差。*/
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	int max = 0;
//	int min = 100;
//	scanf("%d", &n);
//
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &m);
//		if (m > max)
//		{
//			max = m;
//		}
//		if (m < min)
//		{
//			min = m;
//		}
//	}
//
//	printf("%d", max - min);
//	return 0;
//}
/*输入描述：
多组输入，每一行输入一个字母。完成字母大小写转换
输出描述：
针对每组输入，输出单独占一行，输出字母的对应形式。*/
//#include<stdio.h>
//int main()
//{
//	char ch = '0';
//	while ((scanf("%c", &ch)) != EOF)
//	{
//		if (ch >= 'A' && ch <= 'Z')
//		{
//			printf("%c\n", ch + 32);
//		}
//		else if (ch >= 'a' && ch <= 'z')
//		{
//			printf("%c\n", ch - 32);
//		}
//		else
//		{
//			continue;
//		}
//	}
//	return 0;
//}
//#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//
//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d%d", &m, &n);
//	int i, a, b, c;
//	for (i = m; i <= n; i++)
//	{
//		a = i % 10;
//		b = i / 10 % 10;
//		c = i / 100;
//		if (i == a * a * a + b * b * b + c * c * c)
//		{
//			printf("%d\n", i);
//		}
//	}
//	if(m<100||m>1000||n<100||n>1000)
//			printf("Invalid Value.");
//	return 0;
//}
#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//int main() {
//    int a = 0;
//    int N = 0;  // 输入的整数
//    scanf("%d\n", &N);
//    for (int i = 1; i <= N; i++) 
//    {
//        for (int j = 0; j < i; j++)
//        {
//            scanf("%d\n", &i);
//            if (N % i == 0)
//            {
//                printf("Yes\n");
//                continue;
//            }
//            else
//            {
//                printf("No\n");
//                continue;
//            }
//        }
//    }
//    return 0;
//}
/*输入格式：
输入的第一行给出正整数n（1 < n≤10）。随后一行给出n个整数，其间以空格分隔。
	输出格式：
	顺次计算后项减前项之差，并按每行三个元素的格式输出结果。数字间空一格，行末不得有多余空格。
	输入样例：
	10
	5 1 7 14 6 36 4 28 50 100
	输出样例：
	- 4 6 7
	- 8 30 - 32
	24 22 50*/
//#include<stdio.h>
//int main() {
//    int n = 0;
//    scanf("%d", &n);
//    int a[11] = { 0 };
//    for (int i = 0; i < n; i++) {
//        scanf("%d", &a[i]);//读入
//        if (i > 0)
//        {
//            a[i - 1] = a[i] - a[i - 1];
//        }//作差
//    }
//    for (int i = 0; i < n - 1; i++) 
//    {    
//        printf("%d", a[i]);
//        if ((i + 1) % 3 == 0)
//        {
//            printf("\n");
//        }
//        else if (i < n - 2)
//        {
//            printf(" ");
//        } 
//    }
//    return 0;
//}
//#include <stdio.h>
//#include <math.h>
//int main()
//{
//	int n = 0;
//	int flag = 1;
//	int  num = 0;
//	scanf("%d\n", &n);
//	while (n--)
//	{
//		scanf("%d\n", &num);
//		for (int i = 2; i <= sqrt(num); i++)
//		{
//			if (num % i == 0)
//			{
//				flag = 0;
//			}
//			if (num == 1 || flag == 0)
//				printf("Yes\n");
//			else
//				printf("No\n");
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int p = 0;
//	int	i=  0;
//	int num = 0;
//	scanf("%d", &p);
//	while (p--)
//	{
//		scanf("%d\n", &num);
//		for (i = 2; i < num; i++)
//		{
//			if (num % i == 0)
//				a++;
//		}
//			if (a == 0)
//				printf("Yes\n");
//			else
//				printf("No\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//double fact(int a)
//{
//    if (a < 2)
//        return 1;
//    else
//    {
//        return a * fact(a - 1);
//    }
//}
//int main()
//{
//    int m = 0;
//    int n = 0;
//    scanf("%d%d", &m, &n);
//    double s = fact(n);
//    double q = fact(m);
//    double p = fact(n - m);
//    double result = s * 1.0 / (q * p);
//    printf("result=%.0lf\n", result);
//    return 0;
//}
/*输入格式：
输入在一行中给出 24 个 [0, 100] 区间内的整数，依次代表大笨钟在一天 24 小时中，每个小时的心情指数。

随后若干行，每行给出一个 [0, 23] 之间的整数，代表网友询问笨钟这个问题的时间点。当出现非法的时间点时，表示输入结束，这个非法输入不要处理。题目保证至少有 1 次询问。

输出格式：
对每一次提问，如果当时笨钟的心情指数大于 50，就在一行中输出 心情指数 Yes，否则输出 心情指数 No。

输入样例：
80 75 60 50 20 20 20 20 55 62 66 51 42 33 47 58 67 52 41 20 35 49 50 63
17
7
3
15
-1
输出样例：
52 Yes
20 No
50 No
58 Yes*/
#include <stdio.h>
#include <stdlib.h>
int arr[200];
int main()
{
    int s;
    for (int i = 0; i < 24; i++)
    {
        if (i == 0)
        {
            scanf("%d", &arr[i]);
        }
        else
            scanf(" %d", &arr[i]);
    }
    while (1)
    {
        scanf("%d\n", &s);
        if (s == -1)
        {
            return 0;
        }
        else if (s >= 0 && s <= 23)
        {
            if (arr[s] > 50)
            {
                printf("%d Yes\n", arr[s]);
            }
            else
                printf("%d No\n", arr[s]);
        }
        else
            return 0;
    }
}
/*输入格式:
输入在一行中给出若干字符，最后一个回车表示输入结束，不算在内。

输出格式:
在一行内按照

blank = 空格个数, digit = 数字字符个数, other = 其他字符个数
的格式输出。请注意，等号的左右各有一个空格，逗号后有一个空格。

输入样例:
在这里给出一组输入。例如：

Reold 12 or 45T
输出样例:
在这里给出相应的输出。例如：

blank = 3, digit = 4, other = 8*/、
#include<stdio.h>
int main()
{
	int blank = 0;
	int digit = 0;
	int other = 0;
	char ch = '0';
	while ((ch = getchar()) != '\n')
	{
		switch (ch)
		{
		case ' ':
			blank++;
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			digit++;
			break;
		default:
			other++;
		}
	}
	printf("blank = %d, digit = %d, other = %d", blank, digit, other);
	return 0;
}
/*输入格式：
输入的第一行给出正整数n（1<n≤10）。随后一行给出n个整数，其间以空格分隔。

输出格式：
顺次计算后项减前项之差，并按每行三个元素的格式输出结果。数字间空一格，行末不得有多余空格。

输入样例：
10
5 1 7 14 6 36 4 28 50 100
输出样例：
-4 6 7
-8 30 -32
24 22 50*/
