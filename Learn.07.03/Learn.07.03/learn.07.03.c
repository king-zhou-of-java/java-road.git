//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *p);
//		++p;
//	}
//	return 0;
//}
//#include "stdio.h"
//#include "string.h"
//int main()
//{
//	char str1[100];
//	char str2[100];
//	int a, i;
//	gets(str1);
//	a = strlen(str1);
//	for (i = 0; i < a; i++)
//	{
//		str2[i] = str1[a - 1 - i];
//	}
//	str2[i] = 0;//以空字符\0结尾，其ASCII码为0，用来标记字符串的结尾。
//	printf("%s", str2);
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int n = 0;
//	int i = 0;
//	int sum = 0;
//	int tmp = 0;
//	scanf("%d%d", &a, &n);
//	for (i = 0; i < n; i++)
//	{
//		tmp = tmp * 10 + a;
//		sum += tmp;
//	}
//	printf("%d\n", sum);
//	return 0;
//}
#include<stdio.h>
int main()
{
	int i = 0;
	for(i=0; i<=999999; i++)
	{
		int count = 1;
		int tmp = i;
		int sum = 0;
		//判断i是否为水仙花数
		//1. 求判断数字的位数
		while(tmp/10)
		{
			count++;
			tmp = tmp/10;
		}
     
		//2. 计算每一位的次方和
		tmp = i;
		while(tmp)
		{
			sum += pow(tmp%10, count);
			tmp = tmp/10;
		}    
		//3. 判断
		if(sum == i)
			printf("%d ", i);
	}
	return 0;
}
