/*杨氏矩阵
有一个数字矩阵，矩阵的每行从左到右是递增的，
矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。

要求：时间复杂度小于O(N);*/
#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>
struct point
{
	int x;
	int y;
};
struct point find_num(int arr[3][3], int r, int c, int k)
{
	int x = 0;
	int y = c - 1;
	struct point s = { -1,-1 };
	while (x <= r - 1 && y >= 0)
	{
		if (k < arr[x][y])
		{
			y--;
		}
		else if (k> arr[x][y])
		{
			x++;
		}
		else
		{
			s.x = x;
			s.y = y;
			return s;
		}
	}
	return s;//找不到
}
int main()
{
	int k = 0;
	int r = 3;
	int c = 3;
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9};
	scanf("%d", &k);
	struct point ret =  find_num(arr, r, c, k);
	printf("%d %d", ret.x, ret.y);
	return 0;
}