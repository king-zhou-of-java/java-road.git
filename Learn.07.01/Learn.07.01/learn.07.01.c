#define _CRT_SECURE_NO_WARNINGS
//输入一个整数 n ，输出该数32位二进制表示中1的个数。其中负数用补码表示。
//#include<stdio.h>
//int fun(int a)
//{
//	int ret = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if ((a & (1 << i)) != 0)
//		{
//			ret++;
//		}
//	}
//	return ret;
// }
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = fun(n);
//	printf("%d\n", sum);
//	return 0;
////}
// 
// 打印整数二进制的奇数位和偶数位
//#include<stdio.h>
//void print(int m)
//{
//	int i = 0;
//	printf("奇数位:\n");
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d", (m >> i) & 1);
//	}
//	printf("\n");
//	printf("偶数位:\n");
//	for (i = 31; i >= 1; i -= 2)
//	{
//		printf("%d", (m >> i) & 1);
//	}
//	printf("\n");
//}
//int main()
//{
//	int m = 0;
//	scanf("%d", &m);
//	print(m);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a = %d b = %d\n", a, b);
//	return 0;
//}




//统计二进制中1的个数,高级用法
//int count_one_bit(int n)
//{
//	int count = 0;
//	while (n)
//	{
//		n = n & (n - 1);//执行一次去掉一个1
//		//1111 n
//		//1110 n-1
//		//n&(n-1)后的结果下面：
//		//1110  n
//		count++;
//	}
//	return count;
//}



//打印整数二进制的奇数位和偶数位
void Printbit(int num)
{
	//获取奇数位的数字
	for (int i = 31; i >= 1; i -= 2)
	{
		printf("%d ", (num >> i) & 1);
	}
	printf("\n");
	//获取偶数位的数字
	for (int i = 30; i >= 0; i -= 2)
	{
		printf("%d ", (num >> i) & 1);
	}
	printf("\n");
}
