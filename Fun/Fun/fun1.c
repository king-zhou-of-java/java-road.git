#define _CRT_SECURE_NO_WARNINGS
//判断闰年
#include<stdio.h>

int main()
{
	int t = 0;
	int s = 0;
	printf("请输入你要查询的年份：");
	scanf("%d", &t);
	s = fun(t);
	if (s == 1) {
		printf("%d年是闰年",t);
	}
	else
		printf("%d年不是闰年",t);
}
int fun(int x)
{
	if ((x % 4 == 0) && (x % 100 != 0) || x % 400 == 0)
	{
		return 1;
	}
	else
		return 0;
}