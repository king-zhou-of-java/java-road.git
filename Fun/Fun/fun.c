#include<stdio.h>
//判断一个数是否为素数
int main() {
    int i, j;
    printf("请输入一个正整数。\n");
    scanf("%d", &i);
    if (i < 2)
        printf("小于2，请重新输入。\n");
    else if (i % 2 == 0)
        printf("%d不是一个素数。\n", i);
    else {
        for (j = 2; j <= i / 2; j++) {
            if (i % j == 0) {
                printf("%d不是一个素数。\n", i);
                break;
            }
            if (j > i / 2) {
                printf("%d是一个素数。\n", i);
                break;
            }
        }
    }
}
