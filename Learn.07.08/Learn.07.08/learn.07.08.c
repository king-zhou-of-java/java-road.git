////#include<stdio.h>
////int main()
////{
//	//char ch = 'w';
//	//char* p = &ch;
//	//*p = 'b';
//	//printf("%c", ch);
//	//const char* p = "cadad";//把字符串首字符c的地址赋给p  不是存储整个字符串
//	////加const保护了这个字符串  *p不能被改变
//	//char arr[] = "fasdad";//存储整个字符串
//	//int* arr[10];//整型指针的数组   每个元素是int*类型的
//	//char* arr1[10];//一级字符指针数组
//	//char** arr2[8];// 二级字符指针数组
//	/*int arr1[5] = { 1,2,3,4,5 };
//	int arr2[5] = { 2,3,4,5,6 };
//	int arr3[5] = { 3,4,5,6,7 };
//	int* parr[3] = { arr1,arr2,arr3 };
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", *(parr[i] + j));
//		}
//		printf("\n");
//	}*/
//	//数组名通常表示数组首元素的地址 有两个例外
//	//sizeof(数组名）这里的数组名表示整个数组，计算的是整个数组的大小
//	//&数组名，这里的数组名表示整个数组 这里表示取出整个数组的地址
//	//int arr[10] = { 0 };
//	//printf("%p\n", arr);
//	//printf("%p\n", arr+1);
//	//printf("%p\n", &arr);
//	//printf("%p\n", &arr+1);
//	//printf("%p\n", &arr[0]);
//	//printf("%p\n", &arr[0] + 1);
//	//int* p = arr;
//	//int (*p2)[10] = &arr;//数组指针用来存放数组的地址
//	//char* (*pc)[5] = &arr;
//	//二维数组的首元素是它的第一行
//#include<stdio.h>
//
//	void print_2(int(*p)[5], int r, int c)
//	{
//		for (int i = 0; i < r; i++)
//		{
//			for (int j = 0; j < c; j++)
//			{
//				printf("%d ", *(*(p + i) + j));//等价于printf("%d ", arr[i][j]);
//			}
//			printf("\n");
//		}
//	}
//	int main()
//	{
//		int arr[3][5] = { 1,2,3,4,5,2,3,4,5,6,3,4,5,6,7 };
//		print_2(arr, 3, 5);//arr表示第一行
//		return 0;
//	}
//	int arr[5];//arr是整型数组
//	int* parr1[10];//parr是整型指针数组
//	int(*parr2)[10];//parr2是数组指针
//	int(*parr3[10])[5];//parr3是存放数组指针的数组
//
//
////一维数组传参
//#include <stdio.h>
//	void test(int arr[])//ok
//	{}
//	void test(int arr[10])//ok
//	{}
//	void test(int* arr)//ok
//	{}
//	void test2(int* arr[20])//ok
//	{}
//	void test2(int** arr)//ok  
//	//arr2相当于首元素的地址，也就是int *的地址，即二级指针可以用来存放一级指针的地址
//	{}
//	int main()
//	{
//		int arr[10] = { 0 };
//			int* arr2[20] = { 0 };
//		test(arr);
//		test2(arr2);
//	}
//	//二维数组传参
//	void test(int arr[3][5])//ok
//	{}
//	void test(int arr[][])//NO//二维数组行可以省略 列不可以
//	{}
//	void test(int arr[][5])//ok
//	{}
//	//总结：二维数组传参，函数形参的设计只能省略第一个[]的数字。
//	//因为对一个二维数组，可以不知道有多少行，但是必须知道一行多少元素。
//	//这样才方便运算。
//	void test(int* arr)//NO   int (*p)[5]
//	{}
//	void test(int* arr[5])//NO
//	{}
//	void test(int(*arr)[5])//OK
//	{}
//	void test(int** arr)
//	//NO 二维数组的数组名表示首元素的地址 其实是第一行的地址 第一行是一个一维数组
//	{}
//	int main()
//	{
//		int arr[3][5] = { 0 };
//		test(arr);
//	}
//
//	//一级指针传参
//#include <stdio.h>
//	void print(int* p, int sz)
//	{
//		int i = 0;
//		for (i = 0; i < sz; i++)
//		{
//			printf("%d\n", *(p + i));
//		}
//	}
//	int main()
//	{
//		int arr[10] = { 1,2,3,4,5,6,7,8,9 };
//		int* p = arr;
//		int sz = sizeof(arr) / sizeof(arr[0]);
//		//一级指针p，传给函数
//		print(p, sz);
//		return 0;
//	}
//	//函数指针
//#include <stdio.h>
//	void test()
//	{
//		printf("hehe\n");
//	}
//	int main()
//	{
//		printf("%p\n", test);
//		printf("%p\n", &test);
//		//这两个输出等价 对于函数来说 ……函数名和&函数名都是函数的地址
//		return 0;
//	}
//	int Add(int x, int y)
//	{
//		return x + y;
//	}
//	int (*pf)(int, int) = &Add;
//	int ret = (*pf)(2, 3);//这三种写法等价
//	int ret = Add(2, 3);
//	int ret = pf(2, 3);//与Add等价  *可以不写
//#include <stdio.h>
//	int main()
//	{
//		int a = 10;
//		int* p = &a;
//		return 0;
//	}
//
//
