//小乐乐改数字
#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int input = 0;
//	int sum = 0;
//	int i = 0;
//	scanf("%d", &input);
//	while (input)
//	{
//		int s = input% 10;
//		if(s % 2 == 1)
//		{
//			s = 1;
//		}
//		else
//		{
//			s = 0;
//		}
//		sum += s * pow(10, i++);
//		input /= 10;
//	}
//	printf("%d\n", sum);
//	return 0;
//}


//字符串左旋
//#include<stdio.h>
//void reverse_part(char* str, int start, int end) //将字符串从start到end这一段逆序
//{
//	int i, j;
//	char tmp;
//
//	for (i = start, j = end; i < j; i++, j--)
//	{
//		tmp = str[i];
//		str[i] = str[j];
//		str[j] = tmp;
//	}
//}
//
//void leftRound(char* src, int time)
//{
//	int len = strlen(src);
//	int pos = time % len;
//	reverse_part(src, 0, pos - 1); //逆序前段
//	reverse_part(src, pos, len - 1); //逆序后段
//	reverse_part(src, 0, len - 1); //整体逆序
//	printf("%s\n", src);
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	char arr[] = "abcdef";
//	leftRound(arr, 2);
//	return 0;
//}



//柔性数组
//#include<stdio.h>
//struct S
//{
//	int n;
//	int arr[];
//};
//int main()
//{
//	int sz = sizeof(struct S);
//	printf("%d", sz);
//	return 0;
//}



//任何一个C程序，只要运行起来就会默认打开3个流：
//stdin- 标准输入流（键盘）
//stdout- 标准输出流（屏幕）
//stderr 标准错误流（屏幕）
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	FILE* pf = fopen("text.txt", "r");//打开文件
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		perror("fopen");//跟上面一行代码功能相同 但更方便
//		return 1;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c", ch);
//	//fputc('s', pf);
//	fclose(pf);//关闭文件
//	pf = NULL;
//	//sprintf将一个格式化的数据转换成字符串
//	//sscanf
//}
#include <stdio.h>
int main()
{
	int a = 10000;
	FILE* pf = fopen("test.txt", "wb");
	fwrite(&a, 4, 1, pf);//二进制的形式写到文件中
	fclose(pf);				
	pf = NULL;
	return 0;
}