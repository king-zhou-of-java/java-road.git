﻿//字符串左旋的结果
//#include<stdio.h>
//#include<string.h>
//int left_move(char arr1[], char arr2[])
//{
//	int len = strlen(arr1);
//	int i = 0;
//	int tmp = arr1[0];
//	for (i = 0; i < len; i++)
//	{
//		for (int j = 0; j < len - 1; j++)
//		{
//			arr1[j] = arr1[j + 1];
//		}
//	}
//	arr1[len - 1] = tmp;
//	if (strcmp(arr1, arr2) == 0)
//	{
//		return 1;
//	}
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "fedcba";//判断arr2是否可以由arr1旋转得到
//	int ret = left_move(arr1, arr2);
//	if (ret == 1)
//	{
//		printf("ok\n");
//	}
//	else
//		printf("no\n");
//	return 0;
//}





//结构体
//struct Stu
//{
//	int age;
//	char name[];
//}s1,s2;//s1,s2是Stu类型的变量
//int main()
//{
//	struct Stu s3;//s3是局部变量
//}
//
//
//
//
////匿名结构体类型 只能用一次
//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
//
//struct
//{
//	int a;
//	char b;
//	float c;
//}a[20], * p;
//
//int main()
//{
//	p = &x;//编译器会把上面的两个声明当成完全不同的两个类型。所以是非法的
//	return 0;
//}
//
//
////正确的自引用方式
//struct Node
//{
//	int data;
//	struct Node* next;
//};
//
//
//
//
//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
//
//
////初始化：定义变量的同时赋初值。
//struct Point p3 = { x,y};
//
//
//struct Stu     //类型声明
//{
//	char name[15];//名字
//	int age;    //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化 
//
//
//
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化
// 
// 
// 
// 
// 结构体内存对齐
//练习1
//使用offseof函数可以计算成员变量相对于起始位置的偏移量 使用要引用头文件<stddef.h>
//offsetof(struct S ,c1);
//#include<stdio.h>
//struct S1
//
//{
//	char c1;
//	char c2;
//	int i;
//};
//int main()
//{
//	printf("%d\n", sizeof(struct S1));
//}



//练习2
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//printf("%d\n", sizeof(struct S2));
//练习3
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//printf("%d\n", sizeof(struct S3));
//练习4-结构体嵌套问题
struct S4
{
	char c1;
	struct S3 s3;
	double d;
};
printf("%d\n", sizeof(struct S4));
