
//求最大公约数和最小公倍数的和     最小公倍数=（m*n）/最大公约数
#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//int main()
//{
//    long long m;
//    long long n;
//    scanf("%d %d", &m, &n);
//    long long p = m * n;
//    while (n != 0)
//    {
//        long long t = m % n;
//        m = n;
//        n = t;
//    }
//    long long max = m;
//    long long min = p / m;
//    printf("%lld", max + min);
//}


//打印空心正方形图案
//输入：
//4
//复制
//输出：
//* * * *
//*     *
//*     *
//* * * *
//#include<stdio.h>
//void print(int a)
//{
//    int i, j;
//    for (i = 0; i < a; i++)
//    {
//        for (j = 0; j < a; j++)
//        {
//            if (i == 0 || i == a - 1 || j == 0 || j == a - 1)
//            {
//                printf("* ");
//            }
//            else
//                printf("  ");
//        }
//        printf("\n");
//    }
//}
//int main()
//{
//    int a = 0;
//    while (scanf("%d", &a) != EOF)
//    {
//        print(a);
//    }
//    return 0;
//}



//公务员面试
//#include <stdio.h>
//int main()
//{
//	int a, max = 0, small = 100, sum = 0, count = 0;
//	while (scanf("%d", &a) != EOF)
//	{
//		if (a > max)//判定最高分
//		{
//			max = a;
//		}
//		if (a < small)//判定最低分
//		{
//			small = a;
//		}
//		sum += a;
//		count++;//计数器
//		if (count == 7)//计数器=7时代表一组的分数好了可以进行计算
//		{
//			printf("%.2f\n", (sum - max - small) / 5.0);
//			count = 0;//重置
//			max = 0;//重置
//			small = 100;//重置
//			sum = 0;//重置
//		}
//	}
//	return 0;
//}




//打印箭型图案
/*输入：
2
输出：
    *
  **
***
  **
    **/
#include <stdio.h>
int main()
{
	int a;
	while (scanf("%d", &a) != EOF)//符合循环要求
	{
		for (int i = 1; i <= a + 1; i++)//根据题目要求先表示出上半个箭头
		{
			for (int j = 1; j <= (a + 1 - i) * 2 + i; j++)//结合循环和判断确定空格和*的个数
			{
				if (j <= (a + 1 - i) * 2)
				{
					printf(" ");
				}
				else
				{
					printf("*");
				}
			}
			printf("\n");//注意换行符的位置；
		}
		for (int m = 1; m <= a; m++)//同样的方式表示出下半个箭头
		{
			for (int n = 1; n <= m * 2 + a + 1 - m; n++)
			{
				if (n <= (m * 2))
				{
					printf(" ");
				}
				else
				{
					printf("*");
				}
			}
			printf("\n");
		}
	}
	return 0;
}
