//#include<stdio.h>
//int main()
//{
//	char a = 128;
//	printf("%u\n", a);
//}
//#include<stdio.h>
//int main()
//{
//	char a = -128;
//	printf("%u\n", a);
//}
//模拟实现strcpy
//#include<stdio.h>
//void my_strcpy(char* dest, char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;//处理'\0'
//}
//int main()
//{
//	char arr1[20] = "xxxxxxxxxxxxx";
//	char arr2[] = "hello bit";
//	my_strcpy(arr1, arr2);
//	printf("%s", arr1);
//	return 0;
//}
/*输入一个整数数组，实现一个函数，

来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，

所有偶数位于数组的后半部分。*/
#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

void fun(int arr[], int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		while (left < right && arr[left] % 2 == 1)
		{
			left++;
		}
		while (left < right && arr[right] % 2 == 0)
		{
			right--;
		}
		if (left < right)//交换奇数和偶数
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = arr[left];
		}
	}
}
int main()
{
	int arr[10] = { 0 };
		int sz = sizeof(arr) / sizeof(arr[0]);
		for (int i = 0; i < sz; i++)
		{
			scanf("%d", &arr[i]);
		}
		fun(arr, sz);
		for (int i = 0; i < sz; i++)
		{
			printf("%d ", arr[i]);
		}
		return 0;
}
