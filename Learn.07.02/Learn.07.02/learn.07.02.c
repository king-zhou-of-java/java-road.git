#define  _CRT_SECURE_NO_WARNINGS
//打印X型图案
//找到规律是关键，看作一条正斜杠和反斜杠
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)  //外循环为行
//        {
//            for (int j = 0; j < n; j++) //内循环为列
//            {
//                if (i == j || i + j == n - 1)
//                    //最关键的地方，正斜线为[i][i]处是*， 反斜杠为[i][n-1-j]处是*，一行打印1个或2个*
//                    printf("*");
//                else
//                    printf(" ");
//            }
//            printf("\n"); //打印完一行，换行
//        }
//    }
//    return 0;
//}
// //判断一年中每个月有多少天
//#include<stdio.h>
//int fun(int y)
//{
//	return ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0);
//}
//int main()
//{
//	int y = 0;
//	int m = 0;
//	int d = 0;
//	int days[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	while (scanf("%d%d", &y, &m) == 2)
//	{
//		int d = days[m];
//		if (fun(y) == 1 && m == 2)
//		{
//			d++;
//		}
//		printf("%d", d);
//	}
//	return 0;
//}
//针对每组输入数据，输出占一行，如果能构成三角形，等边三角形则输出“Equilateral triangle!”，
//等腰三角形则输出“Isosceles triangle!”，其余的三角形则输出“Ordinary triangle!”，
//反之输出“Not a triangle!”。
//#include<stdio.h>
//int main()
//{
//	int a, b, c;
//	scanf("%d%d%d", &a, &b, &c);
//	if (a + b > c && a - b < c)
//	{
//		if (a == b && b == c)
//		{
//			printf("Equilateral triangle!");
//		}
//		else if (a == b || b == c)
//		{
//			printf("Isosceles triangle!");
//		}
//		else
//			printf("Ordinary triangle!");
//	}
//	else
//		printf("Not a triangle!");
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	unsigned long pulArray[] = { 6,7,8,9,10 };
//	unsigned long* pulPtr;
//	pulPtr = pulArray;
//	*(pulPtr + 3) += 3;
//	printf("%d,%d\n", *pulPtr, *(pulPtr + 3));
//}
//#include <stdio.h>
//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* p = arr;
//	for (int i = 0; i < 10; i++) {
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//将一个字符串str的内容颠倒过来，并输出。
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char str1[100];
//	char str2[100];
//	int a, i;
//	gets(str1);
//	a = strlen(str1);
//	for (i = 0; i < a; i++)
//	{
//		str2[i] = str1[a - 1 - i];
//	}
//	str2[i] = 0;
//	printf("%s", str2);
//}



////求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
////例如：2 + 22 + 222 + 2222 + 22222
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	printf("%d\n", a + (a + a * 10) + (a + a * 10 + a * 100) + (a + a * 10 + a * 100 + a * 1000) + (a + a * 10 + a * 100 + a * 1000 + a * 10000));
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int i;
//	for (i = 0; i < 10000; i++)
//	{
//		int n = 1;
//		int t = i;
//		//1. 计算i的位数 - n
//		while (t / 10)
//		{
//			n++;
//			t = t / 10;
//		}
//		t = i;
//		int sum = 0;
//		//2. 计算i的每一位的n次方之和
//		while (t)
//		{
//			//pow是用来求次方数
//			sum += pow(t % 10, n);
//			t = t / 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//打印菱形
//#include<stdio.h>
//int main()
//{
//    char str[] = "             ";  //定义为空
//
//    int i = 0, j = 0;
//    for (i = (strlen(str) / 2), j = (strlen(str) / 2); i >= 0 && j <= strlen(str); i--, j++)
//    {
//        str[i] = '*';
//        str[j] = '*';
//        printf("%s\n", str);
//    }
//
//    for (i = 0, j = (strlen(str) - 1); i < (strlen(str) / 2) && j >= (strlen(str) / 2); i++, j--)
//    {
//        str[i] = ' ';
//        str[j] = ' ';
//        printf("%s\n", str);
//    }
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//	//初始化
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = i;
//	}
//	//打印
//	for (i = 0; i < 10; i++)
//	{
//		int sum = arr[i];
//		printf("%d ", sum);
//	}
//}
//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编#
#include<stdio.h>
int main()
{
	int a = 1;//汽水一元
	int b = 2;//两个瓶子换一瓶汽水
	int c = 0;//给的钱
	scanf("%d", &c);
	int sum1 = c / a;//得到的瓶子数量
	int sum2 = c / a + sum1/b;
	printf("可以喝%d瓶汽水", sum2);
	return 0; 
}
void my_strcpy(char* dest, char* dest1)
{
	while()
	*dest = *dest1;
	dest++;
	dest1++;
}


char arr1[10] = "hello bite";
char arr2[];
strcpy(arr1, arr2);
printf("%s\n", arr[2]);


